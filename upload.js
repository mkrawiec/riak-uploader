const Riak = require('basho-riak-client')
const winston = require('winston')

module.exports.createTable = cb => {
  const query = `
    CREATE TABLE traffic
    (
      loop         VARCHAR NOT NULL,
      vehicles     SINT64 NOT NULL,
      time         TIMESTAMP NOT NULL,
      PRIMARY KEY (
        (loop, vehicles, QUANTUM(time, 30, 'd')),
        loop, vehicles, time
      )
    )
  `
  return new Riak.Commands.TS.Query.Builder()
    .withQuery(query)
    .withCallback(cb)
    .build()
}

module.exports.insertRows = (rows, next) => {
  return new Riak.Commands.TS.Store.Builder()
    .withTable('traffic')
    .withRows(rows)
    .withCallback((error, result) => {
      logRiakAction(error, result)
      next()
    })
    .build()
}

function logRiakAction(error, result) {
  if (result === true) {
    winston.info('Successfully written to Riak')
  } else {
    winston.error('Error writing to Riak')
  }
}
