const fs = require('fs')
const csv = require('csv-parse')
const walk = require('walk')
const transform = require('stream-transform')
const moment = require('moment')
const path = require('path')
const winston = require('winston')

function formatData(data, filePath) {
  const [loop, time, vehicles] = data
  const date = path.basename(filePath).split('_')[0]
  const timestamp = moment(`${date} ${time}`, 'YYYY-MM-DD hh:mm:ss').toDate()

  return [loop, Number.parseInt(vehicles), timestamp]
}

module.exports.load = (filePath, cb) => {
  const accum = []
  const stream = fs.createReadStream(filePath)
    .pipe(csv({ delimiter: ',' }))
    .on('error', (e) => {
      stream.end()
      winston.error(path.basename(filePath), e.message)
    })
    .pipe(transform(data => formatData(data, filePath)))

  stream.on('readable', () => {
    accum.push(stream.read())
  })

  stream.on('finish', () => {
    accum.pop()
    cb(accum)
  })
}

module.exports.dir = (filePath, cb) => {
  walk.walk(filePath)
    .on('file', cb)
    .on('end', () => winston.info('** All done **'))
}
