const TRAFFIC_PATH = __dirname+'/traffic/'
const LOGGING_PATH = __dirname+'/info.log'

const Riak = require('basho-riak-client')
const winston = require('winston')
const client = new Riak.Client(['10.156.207.188:8087', '10.156.207.48:8087', '10.156.207.150:8087'])
const upload = require('./upload')
const loader = require('./loader')

winston.add(winston.transports.File, { filename: LOGGING_PATH })

function main() {
  loader.dir(TRAFFIC_PATH, (root, stat, next) => {
    loader.load(`${root}/${stat.name}`, rows => {
      if (rows.length > 0) {
        const cmd = upload.insertRows(rows, next)
        client.execute(cmd)
      } else {
        winston.info('Empty file carry on')
        next()
      }
    })
  })
}

client.ping((_, connected) => {
  if (!connected) {
    throw new Error('not connected')
  }

  main()
})
